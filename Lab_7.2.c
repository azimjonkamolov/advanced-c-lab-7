// Name: Lab_7.2.c
// Time: 22:39 11.15.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to check whether a given string is a palindrome or not

#include<stdio.h>
#include<string.h>

int main()
{
	char word[20];
	int i, j, n, flag=0;
	scanf("%s", word);
	n=strlen(word);
	for(i=0,j=n-1;word[i]!='\0';i++,--j)
	{
		if(word[i]==word[j])
		{
			flag=1;
		}
		else
		{
			flag=0;
			break;
		}
	}
	printf("%d", flag);
	return 0;
}
