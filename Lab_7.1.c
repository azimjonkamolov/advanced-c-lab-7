// Name: Lab_7.1.c
// Time: 22:30 11.15.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to call by reverce order and change

#include<stdio.h>
#include<string.h>

int change(int *pa,int *pb, int *pc);

int main()
{
	int a, b, c;
	scanf("%d %d %d", &a, &b, &c);
	change(&a,&b,&c);
	printf("a:%d b:%d c:%d\n", a,b,c);
	
	return 0;
}

int change(int *pa,int *pb, int *pc)
{
	int temp;
	temp=*pa;
	*pa=*pc;
	*pc=*pb;
	*pb=temp;
}
